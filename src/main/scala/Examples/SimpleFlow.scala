package Examples

import akka.stream._
import akka.stream.scaladsl._
import akka.actor.ActorSystem
import akka.stream.scaladsl.GraphDSL.Implicits.SourceArrow
import akka.{Done, NotUsed}
import akka.util.ByteString

import scala.concurrent._
import scala.concurrent.duration._

object FlowExample extends App {

  // An Actor System is required for Materializing, all actor instantiating and processing is handled for you
  // A materializer is the thing that compiles and executes your stream.
  // Without a materializer you can only define a definition of a stream flow/graph you won't be able to run it.
  implicit val system: ActorSystem = ActorSystem("StreamActorSystem")

  // Sources - examples
  val source1 = Source(List(1, 2, 50, 100))
  val source2 = Source.single("My life with Scala...")
  val source3 = Source.empty

  // Flows
  val doubleElements = Flow[Int].map(_ * 2)
  val throttleElementMovement = Flow[Int].throttle(1, 5.seconds)

  // Sinks
  val printElements = Sink.foreach(println _)

  // Examples
  // -------------------

  /*
      Simplest way to connect Stream components is...
      - to link a source to a flow use: .via()
      - to link a flow to a sink use: .to()
      - use .run() to execute the stream
   */

  // No Flow Examples (with immediate execution)
  // In these examples the source is directly connected to a sink: println _, printElements
  // the .run****() executes the stream immediately, this is in contrast to first creating the stream definition and later running it with <streamVariable>.run()
  // Note - these both require a Materializer (in this case it's the implicit ActorSystem declared above)
  source1.runForeach(println _)
  source1.runWith(printElements)

  // Here you can declare a stream definition, this is what's called a 'Runnable Graph'
  val someStreamDefinition = source2.to(printElements)

  // Execute the graph with .run()
  someStreamDefinition.run()


  // Examples With Flows
  val streamDescription = source1.via(doubleElements).to(printElements)
  streamDescription.run()

  val streamProcessing = source1.via(doubleElements).via(throttleElementMovement).runWith(printElements)


  // When you execute this the program won't end immediately because the materializer (ActorSystem) is still up and running.
  // You can stop it by getting a Future result from a stream.
  // For example:
  //  val streamResult = source1.via(doubleElements).runWith(Sink.ignore)
  //  streamResult.onComplete(_ => system.terminate())(system.dispatcher)

}
