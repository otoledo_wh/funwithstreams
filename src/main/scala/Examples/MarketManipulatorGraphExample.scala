package Examples

import akka.actor.ActorSystem
import akka.stream.javadsl.ZipWith
import akka.stream.{ClosedShape, FlowShape}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source, Zip}

case class StockData(sector: String, symbol: String, price: Double)
case class NewStockData(sector: String, symbol: String, oldPrice: Double, newPrice: Double)

// A more complex example... that doesn't really work
object MarketManipulatorGraphExample extends App {
  implicit val system = ActorSystem("GraphExample")

  var stocks = List[StockData]()

  // Get stock data
  val bufferedSource = io.Source.fromFile("./stocks.csv")
  for (line <- bufferedSource.getLines) {
    val cols = line.split(",").map(_.trim)
    stocks = StockData(cols(0), cols(1), cols(2).toFloat) :: stocks
  }
  bufferedSource.close

  def printStocks(x: StockData, y: StockData) = {
    println(s"Sector: ${x.sector}, Symbol: ${x.symbol}, New Price: ${x.price}         ||          Sector: ${y.sector}, Symbol: ${y.symbol}, New Price: ${y.price}")
  }

  // We're going to prepare for an attack on the stock market
  // We're going to take in some stock market data and target 2 sectors
  // Transportation sector - drop price by 0.4 across the board
  // Energy sector - drop the price by 0.25
  val playaaa = GraphDSL.create() (implicit builder => {
    import GraphDSL.Implicits._

    val input = builder.add(Source(stocks))
    val output = builder.add(Sink.foreach[(StockData, StockData)](x => { printStocks(x._1, x._2)}))
    //    val printMe = builder.add(Sink.foreach(println))

    // originally the .filter(....) part of the transportationChange and energyChange Flows.
    // I pulled this out into it's own flow step thinking it might fix a problem but it doesn't
    // For some reason using .filter() breaks the graph.
    // The graph acts as if it executed but nothing gets past the filter steps and nothing reaches the Sink(println).
    // It's almost as if a 1-to-1 mapping for the Zip() tuple is required. If you change transportation below to match the "Energy" keyword, the program executes.
    // This definitely has an answer, Zip() is probably not the right thing to use here. Maybe something fun to try to figure out?

    val transporationFilter = builder.add(Flow[StockData].filter(x => x.sector == "Transportation"))
//    val transporationFilter = builder.add(Flow[StockData].filter(x => x.sector == "Energy"))
    val energyFilter = builder.add(Flow[StockData].filter(sd => sd.sector == "Energy"))
    FlowShape(transporationFilter.in, transporationFilter.out)
    FlowShape(energyFilter.in, energyFilter.out)

    val transporationChange = builder.add(Flow[StockData].map(sd => StockData(sd.sector, sd.symbol, sd.price * 0.4)))
    val energyChange = builder.add(Flow[StockData].map(sd => StockData(sd.sector, sd.symbol, sd.price * 0.25)))
    FlowShape(transporationChange.in, transporationChange.out)
    FlowShape(energyChange.in, energyChange.out)

    val broadcast = builder.add(Broadcast[StockData](2))
    val zip = builder.add(Zip[StockData, StockData])

    // Building the graph shape
    input ~> broadcast.in
    broadcast.out(0) ~> transporationFilter ~> transporationChange ~> zip.in0
    broadcast.out(1) ~> energyFilter ~> energyChange ~> zip.in1
    zip.out ~> output

    ClosedShape
  })

  try {
    // Run the graph
    RunnableGraph.fromGraph(playaaa).run()
  } catch {
    case x: Exception => println(x.getMessage)
  }

}


