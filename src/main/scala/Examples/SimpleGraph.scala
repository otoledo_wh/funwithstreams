package Examples

import akka.actor.ActorSystem
import akka.stream.{ClosedShape, FlowShape}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source, Zip}

// This is a simple example of a graph
object SimpleGraph extends App {

  // the materializer
  implicit val sys = ActorSystem()

  // this is the boilerplate that all graphs follow, you'll put your graph implementation inside the builder function body
//  val aGraph = GraphDSL.create() (implicit builder => { <implement your graph here> })
//  RunnableGraph.fromGraph(aGraph).run()

  val graph = GraphDSL.create() (implicit builder => {
    import GraphDSL.Implicits._ // this allows you to use the 'edge operatior' ~>

    // The main difference in stream component declaration between Flow(Linear) and Graph(Shape), is that with
    // a graph you define you stream step and then you must add it to the 'graph builder' (as done so below)
    // This is so that all input, intermediate steps, and output steps can be known by the graph builder (for correctness checking)
    val source = builder.add(Source(1 to 100))
    val output = builder.add(Sink.foreach(println _))

    val doubler = builder.add(Flow[Int].map(_ * 2))
    val tripler = builder.add(Flow[Int].map(_ * 3))

    // a Broadcast is basically a branch/fan out/fork -ing of some input data.
    // A copy of the input data is sent to each of the specified outputPorts (in the example below we specify branching out to 2 stream components)
    val broadcast = builder.add(Broadcast[Int](2))

    // Zip combines 2 stream components into 1 output, Zip() results in a tuple output
    // Here vvv with [Int, Int] we specify our 2 inputs are of type int, the result wil be a tuple of (Int, Int)
    val zip = builder.add(Zip[Int, Int]())

    // Here we connected the components together
    // the edge operator, ~>, is short hand for writing .via(), .to(), & .connect()
    // Below we say the input from 'source' should be sent to the broadcast component which in turn forks the data
    //  into 2 different branches. Doubler and tripler each apply transformations on the set of in coming data.
    // the result of these Flow transformation steps is then sent to zip by either .in0 or .in1.
    // .in0 and .in1 can be swapped, they just signify the input source, but you cannot send multiple data streams to the same zip input, for example zip.in0
    // the output of Zip (a tuple) is then sent to the Sink

    // Declaring Shape Behavior
    source ~> broadcast.in
    broadcast.out(0) ~> doubler ~> zip.in0
    broadcast.out(1) ~> tripler ~> zip.in1
    zip.out ~> output

    // This declares that the graph should be a considered a 'closed shape',
    // meaning all input and outputs are linked the shape has a beginning and end
    ClosedShape
  })
  // At this point we've declared a graph but it has not been run

  // here we provide the graph definition specified above and declare it to be a 'runnable graph', stating
  // that this graph is closed and can be logically run
  val runnableGraph = RunnableGraph.fromGraph(graph)

  // run the graph
  runnableGraph.run()

}


